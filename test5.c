#include <stdio.h>

void main()
{
    int n;
    printf("Enter the number of elements:");
    scanf("%d",&n);
    int i,j,a[n];
    for(i=0;i<n;i++)
    {
        printf("Enter number %d:",i+1);
        scanf("%d",&a[i]);
    }
    
    int large=a[0],b=0;
    
    for(i=1;i<n;i++)
    {
        if(a[i]>large)
        {
            large=a[i];
            b=i;
        }
    }
   
   int small=a[0],c=0;
   
   for(j=0;j<n;j++)
   {
       if(a[j]<small)
       {
           small=a[j];
           c=j;
       }
   }
   
   a[b]=small;
   a[c]=large;
   
   printf("The resultant array is: {");
   for(i=0;i<n;i++)
   {
       printf("%d",a[i]);
       if(i<n-1)
       {
           printf(",");
       }
       else
       {
           printf("}");
       }
   }
}//write your code here