#include &lt;stdio.h&gt;
void swap_call_val(int, int);
void swap_call_ref(int *, int *);
int main()
{
int a=1, b=2, c=3, d=4;

printf(&quot;\n before swap_call_val, a = %d and b = %d&quot;, a, b);
swap_call_val(a, b);
printf(&quot;\n after swap_call_val, a = %d and b = %d&quot;, a, b);
printf(&quot;\n\n before swap_call_ref, c = %d and d = %d&quot;, c, d);
swap_call_ref(&amp;c, &amp;d);
printf(&quot;\n after swap_call_ref, c = %d and d = %d&quot;, c, d);
return 0;
}
void swap_call_val(int a, int b)
{
int temp;
temp = a;
a = b;
b = temp;
printf(&quot;\n swapping Call By Value Method a = %d and b = %d&quot;, a, b);
}
void swap_call_ref(int *c, int *d)
{
int temp;
temp = *c;
*c = *d;
*d = temp;
printf(&quot;\n swapping Call By Reference Method c = %d and d = %d&quot;, *c, *d);
}//write your code here
